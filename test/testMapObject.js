const mapObject = require("../mapObject");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let cb = () => {
    return "the value is changed."
}
let result = mapObject(testObject,cb);

console.log(result);
