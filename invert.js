function invert(obj) 
{
    let keys = Object.keys(obj);
    let values = Object.values(obj);
    let invert_obj = {};
    for(let index=0; index<keys.length; index++)
    {
        invert_obj[values[index]] = keys[index];
    }
    return invert_obj;
}

module.exports = invert;