function pairs(obj)
{
    let keys = Object.keys(obj);
    let values = Object.values(obj);
    const result = [];
    for(let index=0; index<keys.length; index++)
    {
        result.push([keys[index], values[index]]);
    }
    return result;
}

module.exports = pairs;