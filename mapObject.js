function mapObject(obj, cb) 
{
    let keys = Object.keys(obj);
    for(let index=0; index<keys.length; index++)
    {
        obj[keys[index]] = cb();
    }
    return obj;
}

module.exports = mapObject;