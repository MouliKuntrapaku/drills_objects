function defaults(obj, defaultProps)
{
    let keys = Object.keys(defaultProps);
    let keys_of_obj = Object.keys(obj);
    keys.forEach(element => {
        if(!keys_of_obj.includes(element))
        {
            obj[element] = defaultProps[element];
        }        
    });
    return obj;
}

module.exports = defaults;